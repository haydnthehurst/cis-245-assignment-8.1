import os

def main():

    #Johnny 5
    directory = input("Enter the directory that you want to save the file : ") 
    filename = input("Enter the filename : ")
    name = input("Enter your name : ") 
    address = input("Enter your address : ")
    phone_number = input("Enter your phone number : ") 
    
    #check if the file exists
    if os.path.isdir(directory):

        #creating and open file
        writeFile = open(os.path.join(directory,filename),'w') 

        #writing data
        writeFile.write(name+','+address+','+phone_number+'\n') 
        
        #close the file after writing is done
        writeFile.close() 
        
        #PRINT!
        print("File contents:")

        #reading file
        readFile = open(os.path.join(directory,filename),'r') 
        for line in readFile:
            print(line)

        readFile.close()

    else:
         print("Sorry that directory does not exist")

#Run it 
main()
